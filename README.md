# Projet-reseaux

## Documentation

Raid 5 :  
* Dans le cadre de notre projet on a suivi l'architecture suivante :  
````
+--------------+        +-------+        +------+      +------+
|     HDD1     +        +  HDD2 +        + HDD3 +      + HDD4 |
|      OS      +--------+  25GO +--------+ 25GO +------+ 25GO |
|     10GO     +        +-------+        +------+      +------+ 
+--------------+                                                                     
````
En faisant `lsblk` nous voyons bien les périphériques de stockages ci-dessus. A ce stade ce ne sont que des blocks. 
* Installer lvm : `sudo apt-get install lvm2`
* Nous avons crée trois volumes physiques (pv) : 
    - `pvcreate /dev/sdb /dev/sdc /dev/sdd`
* Nous avons crée un groupe de volume (vg) :
    - `vgcreate vg-raid /dev/sdb /dev/sdc /dev/sdd` 
* Au moment de créer un volume logique (lv) on a indiqué à lvm qu'il s'agissait d'un volume raid 5 : 
    - `lvcreate --size 25G --name lv-raid --type raid5 vg-raid`
* Nous avons ajouté un filesystem (ext4) à notre lv puis nous l'avons monté : 
    - `sudo mkfs.ext4 -L lv-raid /dev/vg-raid/lv-raid`
    - `sudo mount /dev/vg-raid/lv-raid /media/salbrel`
* `sudo nano /etc/fstab` :
     ````
     /dev/mapper/vg--raid-lv--raid/media/salbrel/  ext4    defaults        1       1 
    ````

Borg & borgmatic : 
* Installation de borgbackup : `sudo apt install borgbackup`
* Avoir pip3 et python 3
* `pip3 install wheel`
* `pip3 install --user --upgrade borgmatic`
* `export PATH="$HOME/.local/bin:$PATH"`
* `source ~/.bashrc`
* `sudo env "PATH=$PATH" generate-borgmatic-config`
* `ssh-keygen -t ed25519 -a 100`
* Optionnel :
    - Aller sur le site BorgBase
    - Créer un compte BorgBase
    - Créer un repository BorgBase
    - Renseigner la clé SSH générer au préalable
    - Ajouter le repo
    - Copier le repo path
* Editer le fichier de configuration borgmatic pour sauvegarder le dossier voulu en temps et en heure, on renseigne le repo path à l'interieur.
* `sudo env "PATH=$PATH" validate-borgmatic-config`
* Si ça marche ça renvoi : All given configuration files are valid...
* On initialise le repo `sudo env "PATH=$PATH" borgmatic init --encryption repokey-blake2`
* Création de la première backup : `sudo env "PATH=$PATH" borgmatic --verbosity 1`   

Automatisation des backups avec cron. 

* `sudo visudo` 
* `salbrel ALL=(root) NoPASSWD: /home/salbrel/.local/bin/borgmatic`
* `crontab -e` : on ajoute cette commande à la fin du fichier `* * * * * sudo /home/salbrel/.local/bin/borgmatic` les cinqs étoiles permettent d'appeler le fichier borgmatic toutes les minutes.
* `sudo env "PATH=$PATH" borgmatic list` Liste les backups.
* `cd /media/salbrel/samba/antoinesascha`
* `sudo env "PATH=$PATH" borgmatic extract --archive ...`


Samba : 
* `sudo apt update`
* `sudo apt upgrade`
* `sudo apt install samba`
* `sudo systemctl status smbd` 
* `sudo systemctl restart smbd`
* `sudo systemctl restart nmbd`   
* `sudo mkdir media/salbrel/samba`
* `sudo chgrp sambashare media/salbrel/samba`
* `sudo useradd -M -d media/salbrel/samba/antoinesascha -s /usr/sbin/nologin -G sambashare antoinesascha`
* `sudo mkdir media/salbrel/samba/antoinesascha`
* `sudo chown antoinesascha:sambashare media/salbrel/samba/antoinesascha` définie la propriété du répertoire sur l'utilisateur antoinesascha et le groupe sambashare. 
* `sudo chmod 2770 media/salbrel/samba/antoinesascha` modification des permissions.
* `sudo smbpasswd -a antoinesascha` ajoute le compte utilisateur antoinesascha à la bdd de Samba en définissant le mot de passe utilisateur.
* `sudo smbpasswd -e antoinesascha` permet l'exécution du compte Samba 
* `sudo nano /etc/samba/smb.conf`
````
[antoinesascha]  
    path = media/salbrel/samba/antoinesascha  
    browseable = no  
    read only = no  
    force create mode = 0660 
    force directory mode = 2770  
    valid users = antoinesascha @sambashare
````
* `sudo systemctl restart smbd`
* `sudo systemctl restart nmbd`
